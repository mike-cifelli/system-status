# system-status

A Raspberry Pi project to show a simple display of the system status. A Unicorn HAT Mini is used to display the temperature, cpu level and network connections.

## Dependencies
```
sudo raspi-config nonint do_spi 0
sudo apt install python3-pip
sudo apt install python3-gpiozero
sudo pip3 install unicornhatmini
```

## Installation
```
sudo ./install
```
